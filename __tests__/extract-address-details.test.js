const { expect, test } = require('@jest/globals');
const extractAddressDetails = require('../src/tasks/extract-address-details');

test('extract address details', () => {
  expect(extractAddressDetails)
    .toEqual([
      ['#204 10123 157 STREET', 'Edmonton', 'Alberta', 'T5P2T9'],
      ['10950 117 Street NW', 'Edmonton', 'Alberta', 'T5H3N6'],
      ['9330 14th Ave S.W', 'Calgary', null, 'T3H0M1'],
      ['117- 4950 106th Avenue SE', 'Calgary', 'Alberta', 'T2C5E9'],
      ['1215 - 2nd Avenue South', 'Lethbridge', 'Alberta', 'T1J0E4'],
      ['74 Mt Alberta Mr SE', 'Calgary', 'Alberta', 'T2Z3J4'],
      ['527 Butterworth Way Edmonton Alberta T6R 2N4 Canada', 'Edmonton', 'Alberta', 'T6R2N4'],
      ['527 Butterworth Way Edmonton Alberta T6R 2N4 Canada', 'Edmonton', 'Alberta', 'T6R2N4'],
      ['Box 1600', 'Fort Macleod', 'Alberta', 'T0L0Z0'],
      ['2500-500 4 Ave SW', null, null, null],
      ['Suite 202, 1077 - 56th Street', 'Delta', 'British Columbia', 'V4L2A2'],
      ['5838 Cambie St', 'Vancouver', 'British Columbia', 'V5Z3A8'],
      ['32 WHITMAN CLOSE NE', 'Calgary', 'Alberta', 'T1Y4H4'],
      ['16 York Street, Suite 1900', 'Toronto', 'Ontario', 'M5J0E6'],
      ['#1100 - 1040 WEST GEORGIA STREET', 'Vancouver', 'British Columbia', 'V6E4H1'],
      ['8th Floor - 2425 Matheson Blvd. E.', 'Mississauga', 'Ontario', 'L4W5K4'],
      ['150 9th Ave SW Suite 2100', 'Calgary', 'Alberta', 'T2P1B4'],
      ['2100-2200 rue Stanley', 'Montréal', 'Québec', 'H3A1R6'],
      ['2215B COQUITLAM AVE.', 'Port Coquitlam', 'British Columbia', 'V3B1J6'],
      ['307 South Harmony Drive', 'Rockyview County', 'Alberta', 'T3Z0E6'],
      ['436 McDonald Street', 'Regina', 'Saskatchewan', 'S4N6E1'],
      ['1915 Foxtail Terrace', 'Kelowna', 'British Columbia', 'V1P1T9'],
      ['1778 6 AVENUE NW', 'Calgary', 'Alberta', null],
      [null, null, null, null],
    ]);
});
