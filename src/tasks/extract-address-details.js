const _ = require('lodash');
const fs = require('fs')
const coll = JSON.parse(fs.readFileSync('./__fixtures__/addresses.json', 'utf-8'));

const provinces = {
  AB: 'ALBERTA',
  BC: 'BRITISH COLUMBIA',
  MB: 'MANITOBA',
  NB: 'NEW BRUNSWICK',
  NL: 'NEWFOUNDLAND AND LABRADOR',
  NS: 'NOVA SCOTIA',
  ON: 'ONTARIO',
  PE: 'PRINCE EDWARD ISLAND',
  QC: 'QUEBEC',
  SK: 'SASKATCHEWAN',
};

const provincesRe = [
  /ALBERTA[\s,]/i,
  /BRITISH COLUMBIA[\s,]/i,
  /MANITOBA[\s,]/i,
  /NEW BRUNSWICK[\s,]/i,
  /NEWFOUNDLAND.*LABRADOR[\s,]/i,
  /NOVA SCOTIA[\s,]/i,
  /ONTARIO[\s,]/i,
  /PRINCE EDWARD ISLAND[\s,]/i,
  /QUEBEC[\s,]/i,
  /SASKATCHEWAN[\s,]/i,
  /AB[\s,]/,
  /BC[\s,]/,
  /MB[\s,]/,
  /NB[\s,]/,
  /NL[\s,]/,
  /NS[\s,]/,
  /ON[\s,]/,
  /PE[\s,]/,
  /QC[\s,]/,
  /SK[\s,]/,
];

const canadianPostCodeRe = /[A-Z]\d[A-Z]\s?\d[A-Z]\d/;

/**
 * The 'extractAddressDetails' function takes a single parameter 'fullAddress',
 * which is a string, includes a street address, city, province and zipcode
 * of some canadian location.
 *
 * The function should parse the string and build an array with 4 elements:
 * street address, city, province, zipcode.
 * If any part is dissmised in source string, set null as it value.
 * Also you should capitalize city and province. 
 *
 * Pay attention:
  * different strings have different style of province naming;
  * street address is always separated from city by <br> tag;
  * city, province and zipcode are always together without <br> among them;
  * country is not always specified;
  * zipcode sometimes can include a space inside and should br normalized without it;
  * zipcode sometimes can include lower letters inside.
  *
  * String example:
    [
      '#204 10123 157 STREET<br>EDMONTON ALBERTA T5P 2T9<br>Canada',
      '10950<br>117 Street NW<br>Edmonton, Alberta T5H 3N6',
    ]
  *
  * Expected result:
    [
      ['#204 10123 157 STREET', 'Edmonton', 'Alberta', 'T5P2T9'],
      ['10950 117 Street NW', 'Edmonton', 'Alberta', 'T5H3N6'],
    ]
 */

const extractAddressDetails = (fullAddress) => {
    // put your code here
}

module.exports = _.map(coll, extractAddressDetails);
